#include <iostream>
#include <chrono>
#include <ctime>
#include <fstream>
// #include <string.h>
using namespace std;


double run2by2(int iterations){
    // std::cout << iterations << "\n";
    int base = 2;
    auto start = std::chrono::system_clock::now();
    for(long i = 0; i <= iterations; i++)
    {
        base = base * 2;
    }
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::time_t end_time = std::chrono::system_clock::to_time_t(end);

    // std::cout << "elapsed time: " << elapsed_seconds.count() << "s\n";
    return elapsed_seconds.count();
}

void writeCSV(int iterations, double time){
    ofstream csv;
    csv.open("trialsCpp.csv", ios_base::app);
    csv << time << "," << iterations << "\n";
    csv.close();
}


int main(int argc, char *argv[]){
    printf("starting\n");
    for(long it = 50000; it <= 1000000; it += 1000)
    // for(long it = 50000; it <= 1000000; it = it + 1000)
    {
        double sum = 0.00000000;
        for(int r = 0; r < 10; r++)
        {
            sum += double(run2by2(it));
            // cout << "sum: " << sum;
        }
        double average = sum /10;
        writeCSV(it, average);
        cout << "Finished Repetition with " << it << " iterations. Average Time:" << average << "s\n";
    }
    return 0;
}

