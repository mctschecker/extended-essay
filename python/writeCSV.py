import csv

def writeCSV(time,iterations):
    with open(r"trials.csv", "a") as f: #opens csv in apend mode
        csv.writer(f).writerow([time, iterations])