import firebase_admin
from firebase_admin import credentials, firestore
import json
import random
import string

cred = credentials.Certificate("./adminCert.json")
default_app = firebase_admin.initialize_app(cred, {
    'databaseURL' : 'https://extended-essay-234820.firebaseio.com'
})

db = firebase_admin.firestore.client()

def updateFirebase(time, iterations):
    data = {
        "iterations": iterations,
        "time": time,
        "isPython": True
    }
    documentID = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(30))
    db.document("2*2/" +documentID).set(data)