#Main file to run all the tests
from speedMult import mult
from writeCSV import writeCSV

def main():
        #runs 10 times to ensure
        for i in range(50000,1000000, 1000):
                list =[]
                for i in range(0, 100):
                        list += [mult(i, 2, 2)]
                writeCSV(sum(list)/len(list), i)
