import time


def mult(iterations, base, factor):
    beginning = time.time()
    for i  in range(0,iterations):
        base = base * factor
    timeTook = time.time()-beginning
    return timeTook